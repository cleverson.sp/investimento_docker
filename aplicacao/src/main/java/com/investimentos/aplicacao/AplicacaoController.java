package com.investimentos.aplicacao;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class AplicacaoController {

	@Autowired
	AplicacaoService aplicacaoService;

	@PostMapping
	@ResponseStatus(code = HttpStatus.CREATED)
	public Investimento criar(@RequestBody Investimento investimento) {
		Optional<Investimento> optional = aplicacaoService.criar(investimento);

		if (optional.isPresent()) {
			return optional.get();
		}

		throw new ResponseStatusException(HttpStatus.NOT_FOUND);
	}

}
